frappe.provide("erpnext.item");

frappe.ui.form.on("Item", {
  refresh: function (frm) {
    if (frm.doc.docstatus == '0') {
      frm.add_custom_button(__("Save"), function () {
        frm.events.save(frm);
      }).addClass("btn-primary");
    }
    frm.refresh_fields();
  },

  save: function (frm) {
    return frappe.call({
      doc: frm.doc,
      method: "nodux_product.product.item.validate",
      freeze: true,
      callback: function (r) {
        frm.refresh_fields();
        frm.refresh();
      }
    })
  },

  customer: function (frm) {
    if (frm.doc.customer) {
      frm.set_value("customer_name", frm.doc.customer);
      frm.set_value("due_date", frappe.datetime.nowdate());
    }
    frm.refresh_fields();
  },

  item_code: function (frm) {
    if (frm.doc.item_code == frm.doc.item_name) {
      frm.set_value("item_name", "");
      frm.set_value("description", "");
    }
  },

  precio_1: function (frm) {
    var doc = frm.doc;
    doc.precio_1_con_imp = get_precio_mas_impuesto(frm, doc.precio_1);
    frm.refresh_fields();
  },

  precio_1_con_imp: function (frm) {
    var doc = frm.doc;
    doc.precio_1 = get_precio_menos_impuesto(frm, doc.precio_1_con_imp);
    frm.refresh_fields();
  },

  list_price: function (frm) {
    var doc = frm.doc;
    doc.list_price_with_tax = get_precio_mas_impuesto(frm, doc.list_price);
    frm.refresh_fields();
  },

  list_price_with_tax: function (frm) {
    var doc = frm.doc;
    doc.list_price = get_precio_menos_impuesto(frm, doc.list_price_with_tax);
    frm.refresh_fields();
  },

  precio_2_sin_imp: function (frm) {
    var doc = frm.doc;
    doc.precio_2_con_imp = get_precio_mas_impuesto(frm, doc.precio_2_sin_imp);
    if (doc.cantidad > 0) {
      doc.precio_por_caja = doc.precio_2_con_imp * doc.cantidad;
    }
    frm.refresh_fields();
  },

  precio_2_con_imp: function (frm) {
    var doc = frm.doc;
    doc.precio_2_sin_imp = get_precio_menos_impuesto(frm, doc.precio_2_con_imp);
    if (doc.cantidad > 0) {
      doc.precio_por_caja = doc.precio_2_con_imp * doc.cantidad;
    }
    frm.refresh_fields();
  },

  cantidad: function (frm) {
    var doc = frm.doc;
    if (doc.precio_por_caja > 0 & doc.cantidad > 0) {
      doc.precio_2_con_imp = flt(doc.precio_por_caja) / flt(doc.cantidad);
      doc.precio_2_sin_imp = flt(get_precio_menos_impuesto(frm, doc.precio_2_con_imp));
    } else {
      if (!doc.cantidad > 0) {
        msgprint('La Cantidad debe ser un <b>número</b> mayor a <b>"0"</b>');
        doc.cantidad = "";
        doc.precio_2_con_imp = "";
        doc.precio_2_sin_imp = "";
      }
    }
    frm.refresh_fields();
  },

  precio_por_caja: function (frm) {
    var doc = frm.doc;
    if (doc.cantidad > 0 & doc.precio_por_caja > 0) {
      doc.precio_2_con_imp = flt(doc.precio_por_caja) / flt(doc.cantidad);
      doc.precio_2_sin_imp = flt(get_precio_menos_impuesto(frm, doc.precio_2_con_imp));
    } else {
      if (!doc.precio_por_caja > 0) {
        msgprint('El Precio por Caja debe ser un <b>número</b> mayor a <b>"0"</b>');
        doc.precio_por_caja = "";
        doc.precio_2_con_imp = "";
        doc.precio_2_sin_imp = "";
      }
    }
    frm.refresh_fields();
  },

  cost_price: function (frm) {
    var doc = frm.doc;
    doc.cost_price_with_tax = get_precio_mas_impuesto(frm, doc.cost_price);
    frm.refresh_fields();
  },

  cost_price_with_tax: function (frm) {
    var doc = frm.doc;
    doc.cost_price = get_precio_menos_impuesto(frm, doc.cost_price_with_tax);
    frm.refresh_fields();
  },

  item_group: function (frm) {
    var group = frm.doc.item_group;
    if (group) {
      frappe.db.get_value("Item Group", {
        "item_group_name": group
      }, "tax", function (r) {
        frm.set_value("tax", r.tax);
      })
    }
    frm.refresh_fields();
  },

  tax: function (frm) {
    var doc = frm.doc;
    if (doc.precio_1 > 0) {
      doc.precio_1_con_imp = get_precio_mas_impuesto(frm, doc.precio_1);
    }
    if (doc.list_price > 0) {
      doc.list_price_with_tax = get_precio_mas_impuesto(frm, doc.list_price);
    }
    if (doc.precio_2_sin_imp > 0) {
      doc.precio_2_con_imp = get_precio_mas_impuesto(frm, doc.precio_2_sin_imp);
    }
    if (doc.cost_price > 0) {
      doc.cost_price_with_tax = get_precio_mas_impuesto(frm, doc.cost_price);
    }
    frm.refresh_fields();
    /*var cost_price_with_tax = 0
    var list_price_with_tax = 0
    var precio_1_con_imp = 0
    if (frm.doc.tax) {
      if (frm.doc.tax == "IVA 12%") {
        if (frm.doc.cost_price) {
          cost_price_with_tax = flt(frm.doc.cost_price) * flt(1 + 0.12)
        }
        if (frm.doc.list_price) {
          list_price_with_tax = flt(frm.doc.list_price) * flt(1 + 0.12)
        }
        if (frm.doc.list_price) {
          precio_1_con_imp = flt(frm.doc.precio_1_con_imp) * flt(1 + 0.12)
        }
      }
      if (frm.doc.tax == "IVA 0%") {
        if (frm.doc.cost_price) {
          cost_price_with_tax = flt(frm.doc.cost_price)
        }
        if (frm.doc.list_price) {
          list_price_with_tax = flt(frm.doc.list_price)
        }
      }
      if (frm.doc.tax == "No aplica impuestos") {
        if (frm.doc.cost_price) {
          cost_price_with_tax = flt(frm.doc.cost_price)
        }
        if (frm.doc.list_price) {
          list_price_with_tax = flt(frm.doc.list_price)
        }
      }

      frm.set_value("cost_price_with_tax", cost_price_with_tax);
      frm.set_value("list_price_with_tax", list_price_with_tax);
      frm.set_value("precio_1_con_imp", precio_1_con_imp);

    }
    frm.refresh_fields();*/
  },
});

/*var calculate_prices = function (frm) {
  var doc = frm.doc;
  doc.list_price_with_tax = 0;
  doc.cost_price_with_tax = 0;
  cost_price_with_tax = 0;
  list_price_with_tax = 0;

  if (doc.tax) {
    if (doc.tax == "IVA 12%") {
      if (doc.cost_price) {
        cost_price_with_tax = flt(doc.cost_price) * flt(1 + 0.12)
      }
      if (doc.list_price) {
        list_price_with_tax = flt(doc.list_price) * flt(1 + 0.12)
      }
    }
    if (doc.tax == "IVA 0%") {
      if (doc.cost_price) {
        cost_price_with_tax = flt(doc.cost_price)
      }
      if (doc.list_price) {
        list_price_with_tax = flt(doc.list_price)
      }
    }
    if (doc.tax == "No aplica impuestos") {
      if (doc.cost_price) {
        cost_price_with_tax = flt(doc.cost_price)
      }
      if (doc.list_price) {
        list_price_with_tax = flt(doc.list_price)
      }
    }
    doc.list_price_with_tax = list_price_with_tax;
    doc.cost_price_with_tax = cost_price_with_tax;
  } else {
    if (doc.group) {
      frappe.db.get_value("Item Group", {
        "item_group_name": doc.group
      }, "tax", function (r) {
        if (r.tax == "IVA 12%") {
          if (doc.cost_price) {
            cost_price_with_tax = flt(doc.cost_price) * flt(1 + 0.12)
          }
          if (doc.list_price) {
            list_price_with_tax = flt(doc.list_price) * flt(1 + 0.12)
          }
        } else if (r.tax == "IVA 0%") {
          if (doc.cost_price) {
            cost_price_with_tax = flt(doc.cost_price)
          }
          if (doc.list_price) {
            list_price_with_tax = flt(doc.list_price)
          }
        } else if (r.tax == 'No aplica impuestos') {
          if (doc.cost_price) {
            cost_price_with_tax = flt(doc.cost_price)
          }
          if (doc.list_price) {
            list_price_with_tax = flt(doc.list_price)
          }
        }
      })
      doc.list_price_with_tax = list_price_with_tax;
      doc.cost_price_with_tax = cost_price_with_tax;
    }
  }
  refresh_field('list_price_with_tax')
  refresh_field('cost_price_with_tax')
}

var calculate_prices_with_tax = function (frm) {
  var doc = frm.doc;
  doc.list_price = 0;
  doc.cost_price = 0;
  cost_price = 0;
  list_price = 0;

  if (doc.tax) {
    if (doc.tax == "IVA 12%") {
      if (doc.cost_price_with_tax) {
        cost_price = flt(doc.cost_price_with_tax) / flt(1 + 0.12)
      }
      if (doc.list_price_with_tax) {
        list_price = flt(doc.list_price_with_tax) / flt(1 + 0.12)
      }
    } else if (doc.tax == "IVA 0%") {
      if (doc.cost_price_with_tax) {
        cost_price = flt(doc.cost_price_with_tax)
      }
      if (doc.list_price_with_tax) {
        list_price = flt(doc.list_price_with_tax)
      }
    } else if (doc.tax == "No aplica impuestos") {
      if (doc.cost_price_with_tax) {
        cost_price = flt(doc.cost_price_with_tax)
      }
      if (doc.list_price_with_tax) {
        list_price = flt(doc.list_price_with_tax)
      }
    } else {
      if (doc.cost_price_with_tax) {
        cost_price = flt(doc.cost_price_with_tax)
      }
      if (doc.list_price_with_tax) {
        list_price = flt(doc.list_price_with_tax)
      }
    }
    doc.list_price = list_price;
    doc.cost_price = cost_price;
  } else {
    if (doc.group) {
      frappe.db.get_value("Item Group", {
        "item_group_name": doc.group
      }, "tax", function (r) {
        if (r.tax == "IVA 12%") {
          if (doc.cost_price_with_tax) {
            cost_price = flt(doc.cost_price_with_tax) / flt(1 + 0.12)
          }
          if (doc.list_price_with_tax) {
            list_price = flt(doc.list_price_with_tax) / flt(1 + 0.12)
          }
        } else if (r.tax == "IVA 0%") {
          if (doc.cost_price_with_tax) {
            cost_price = flt(doc.cost_price_with_tax)
          }
          if (doc.list_price_with_tax) {
            list_price = flt(doc.list_price_with_tax)
          }
        } else if (r.tax == 'No aplica impuestos') {
          if (doc.cost_price_with_tax) {
            cost_price = flt(doc.cost_price_with_tax)
          }
          if (doc.list_price_with_tax) {
            list_price = flt(doc.list_price_with_tax)
          }
        } else {
          if (doc.cost_price_with_tax) {
            cost_price = flt(doc.cost_price_with_tax)
          }
          if (doc.list_price_with_tax) {
            list_price = flt(doc.list_price_with_tax)
          }
        }
      })
      doc.list_price = list_price;
      doc.cost_price = cost_price;
    }
  }
  refresh_field('list_price');
  refresh_field('cost_price');
}*/

var get_precio_mas_impuesto = function (frm, precio) {
  var doc = frm.doc;
  if (doc.tax) {
    if (doc.tax == "IVA 12%") {
      return flt(precio) * flt(1 + 0.12);
    }
    if (doc.tax == "IVA 0%") {
      return flt(precio);
    }
    if (doc.tax == "No aplica impuestos") {
      return flt(precio);
    }
  }
}

var get_precio_menos_impuesto = function (frm, precio) {
  var doc = frm.doc;
  if (doc.tax) {
    if (doc.tax == "IVA 12%") {
      return flt(precio) / flt(1 + 0.12);
    }
    if (doc.tax == "IVA 0%") {
      return flt(precio);
    }
    if (doc.tax == "No aplica impuestos") {
      return flt(precio);
    }
  }
}